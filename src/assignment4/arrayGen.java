package assignment4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/**
Class that contains main for running the different searching algorithms. 
@author Sean Gajjar
@version 1.0 2014-02-11
*/

public class arrayGen
{
	static int badValues = 0; //global count of bad values from the input file 
	static long e1t, e2t, e3t, e4t, e5t; //all of the times for each experiment
	static int valuesFoundGlobal = 0;
	static int valuesNotFoundGlobal = 0;
	
	/* 
	 * Main class, runs each and every experiment. 
	 * */
	public static void main(String[] args) throws IOException 
	{
		ArrayList<Integer> inputtedFiles = new ArrayList<Integer>();
		if (args.length != 1) 
		{
			System.err.println ("Error: Incorrect number of command line arguments");
			System.exit(-1);
		} 
		processLinesInFile(args[0], inputtedFiles);	// process the input file already please
													//at this point we have every single value from the input file stored.
		ArrayList<Integer> emp = new ArrayList<Integer>(1000000);
		generate(emp);								//generate the random array
//		inputtedFiles.add(0, emp.get(899));
		experiment1(inputtedFiles,emp);
		experiment2(inputtedFiles,emp);
		experiment3(emp);
		experiment4(emp, inputtedFiles);
		experiment5(emp, inputtedFiles);
		printEverything();
		
	}
	
	/*
	 * loads all of the input data
	 * checks to see if all are integers
	 * if not, it skips the nonintegers
	 * if yes, then adds the integers to an arrayList
	 * 
	 * @param filename is the input.txt
	 * @param checkWith is the arrayList the input values are added to
	 * 
	 */
	public static void processLinesInFile (String filename, ArrayList<Integer> checkWith) throws IOException 
	{ 
		StopWatch stopwatch = new StopWatch();

		arrayGen myPalFinder = new arrayGen(); 
		int temp;
		FileReader freader = new FileReader(filename);
		BufferedReader reader = new BufferedReader(freader);
		stopwatch.start(); 
	
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{
				try 
				{
					temp = Integer.parseInt(s);
					if(temp>0 && temp <= Integer.MAX_VALUE)
					{
					checkWith.add(temp);
					}
					else 
					{
						badValues++;
					}
				}
				catch(NumberFormatException e)
				{
					badValues++;
				}
			}
			System.out.println("TIME TO READ INPUT... ");
			System.out.println(stopwatch.getElapsedTime() + " nanoseconds.");
			stopwatch.stop();
			stopwatch.reset();
				
		 
		
		
		
	}
	
	/*
	 * checks if the string is a valid input
	 * by checking against length, pegs, and color options
	 * 
	 * @param empty the arrayList that is filled with 10000000 random values
	 */
	public static void generate(ArrayList<Integer> empty) //generate's the 1283912218932 element array.
	{
		StopWatch stopwatch = new StopWatch();
		
		Random randomObj = new Random(15485863);
		int cow;
		stopwatch.start();
		for(int i = 0; i < 1000000; i++) //change the number here for however many array elements you want
		{
		
		cow = randomObj.nextInt(Integer.MAX_VALUE);
		if(cow <0){cow *= -1;} // make the number positive
		empty.add(cow);
		//System.out.println(cow);
		}
//		int cowsay;
//	
//		for(int j = 0; j< 1000; j++)
//		{
//
//			cowsay = randomObj.nextInt(Integer.MAX_VALUE);
//			if(cowsay <0){cowsay *= -1;} // make the number positive
//			System.out.println(cowsay);
//		}
		System.out.println("GENERATING 1 MILLION VALUES IN ARRAYLIST... ");
		System.out.println(stopwatch.getElapsedTime() + " nanoseconds.");
		stopwatch.stop();
		stopwatch.reset();
	}
	
	/*
	 * checks if the string is a valid input
	 * by checking against length, pegs, and color options
	 * 
	 * @param processed the inputtedFiles are passed into here. This contains the input.txt values
	 * @param generated is the total generated random numbers that are created at the beginning of the program
	 * 
	 */
	public static void experiment1(ArrayList<Integer> processed, ArrayList<Integer> generated) //pass in the two arrays to go through
	{
		int valuesFound = 0;
		int valuesNotFound= 0;
		StopWatch stopwatch = new StopWatch();
	
		boolean isIt = false;
		
		for(Integer i:processed)
		{
			
			
			stopwatch.start(); 
			for(Integer f: generated)
			{
				if(i.intValue()==f.intValue())
				{
					stopwatch.stop();
					System.out.println("Found at : " + generated.indexOf(f) + " at this time : " + stopwatch.getElapsedTime());
					if(isIt==false){valuesFound+=1;} //remove this if you want to count duplicates as diff values. 
					isIt = true;
					
					stopwatch.start();
				}
				
			}
			if(!isIt)
			{
				valuesNotFound++;
				stopwatch.stop();
				System.out.println("Nothing found but it took this long : " + stopwatch.getElapsedTime());
			}
			isIt = false;
		}
		e1t = stopwatch.getElapsedTime(); 
		System.out.println(valuesFoundGlobal);
		valuesFoundGlobal = valuesFound;
//		System.out.println(valuesFoundGlobal);
		valuesNotFoundGlobal = valuesNotFound;
	}
	
	/*
	 * Next convert your ArrayList into a LinkedList in the same order; you may use the JCF LinkedList class for this purpose. 
	 * Reopen the disk file and perform the series of searches for values as before.  
	 * Measure, record and report the total search time (again without I/O time).  Close the disk file.
	 * 
	 * @param processed the inputtedFiles are passed into here. This contains the input.txt values
	 * @param generated is the total generated random numbers that are created at the beginning of the program
	 * 
	 */
	public static void experiment2(ArrayList<Integer> processed, ArrayList<Integer> generated)
	{
		LinkedList<Integer> processedLL = new LinkedList<Integer>();
		LinkedList<Integer> generatedLL = new LinkedList<Integer>();
		

		for(Integer i:processed) //potentially use an addAll()?
		{
			
			processedLL.add(i);
		}
		
		for(Integer j: generated)
		{
			generatedLL.add(j);
		}
		experiment22(processedLL,generatedLL);
		
	}
	
	public static void experiment22(LinkedList<Integer> processedLL, LinkedList<Integer> generatedLL)
	{
		StopWatch stopwatch = new StopWatch();
		
		boolean isIt = false;
		
		
		for(Integer i:processedLL)
		{
			stopwatch.start(); 
			for(Integer f: generatedLL)
			{
			
				if(i.intValue()==f.intValue())
				{
					stopwatch.stop(); 
					System.out.println("LinkedList location found at : " + generatedLL.indexOf(f) + " at this time : " + stopwatch.getElapsedTime());
					isIt = true;
				}
			}
			if(!isIt)
			{
				stopwatch.stop(); 
				System.out.println("Nothing found but it took this long : " + stopwatch.getElapsedTime());
			}
			isIt = false;
		}
		e2t = stopwatch.getElapsedTime(); 
	}
	
	 
	/*
	 * You will process each value from the input file in turn
	 * search the ArrayList sequentially, and then report the 
	 * index where the value was found, or report that it was not found.
	 * Do this for all values in the disk file. 
	 *
	 * @param generated is the total generated random numbers that are created at the beginning of the program
	 * 
	 */
	public static void experiment3(ArrayList<Integer> generatedSort)
	{
		StopWatch stopwatch = new StopWatch();
		stopwatch.start();
		Collections.sort(generatedSort);
		stopwatch.stop();
		e3t = stopwatch.getElapsedTime();
	}
	
	/*
	 * Next reopen the file on disk, and again process each value in turn,
	 * searching the ordered ArrayList using the binarySearch method from the API.  
	 * Measure and report the total time spent on searching for these values
	 * (not including I/O time to read or report).   Close the disk file.
	 *
	 * @param processeded the inputtedFiles are passed into here. This contains the input.txt values
	 * @param generateded is the total generated random numbers that are created at the beginning of the program
	 * 
	 */
	public static void experiment4(ArrayList<Integer> generateded, ArrayList<Integer> processeded)
	{
		StopWatch stopwatch = new StopWatch();
		
		
		for(Integer i: processeded)
		{
			long timeCheck;
			stopwatch.start(); 
			int index = Collections.binarySearch(generateded, i);
			stopwatch.stop(); 
			timeCheck = stopwatch.getElapsedTime();
			if(index>=0)
			{
			System.out.println("Found at index for a sorted arrayList:" + index +" at time : " + timeCheck);
			}
			else
			{
				System.out.println("Nothing found in sorted arraylist: ");
			}
		}
		e4t = stopwatch.getElapsedTime(); 
	}
	
	/*
	 * Interpolation Search. 
	 *
	 * @param processeded the inputtedFiles are passed into here. This contains the input.txt values
	 * @param generateded is the total generated random numbers that are created at the beginning of the program
	 * 
	 */
	public static void experiment5(ArrayList<Integer> generateded, ArrayList<Integer> processeded) //make this iterate and then pass off to search to do the job. 
	{
		StopWatch stopwatch = new StopWatch();
		
		int totalFound=0;
		int valuesNotFound = 0;
		
		for(Integer i: processeded)
		{
			valuesNotFound++;
			long timeCheck;
			stopwatch.start(); 
			int index = interpolationSearch(generateded, i);
			stopwatch.stop(); 
			timeCheck = stopwatch.getElapsedTime();
			if(index>=0)
			{
				totalFound++;
			System.out.println("Found at index for a sorted arrayList interpolation:" + index +" at time : " + timeCheck);
			}
			else
			{
				System.out.println("Didnt find in sorted interpolation :( ");
			}
		}
		e5t = stopwatch.getElapsedTime(); 
		stopwatch.reset();
	}
	
	public static int interpolationSearch(ArrayList<Integer> sortedArray, int toFind){
		  // Returns index of toFind in sortedArray, or -1 if not found
		  int low = 0;
		  int high = sortedArray.size() - 1;
		  int mid;
		 
		  while (sortedArray.get(low) <= toFind && sortedArray.get(high) >= toFind) {
			  int a = (int)(toFind - (double)sortedArray.get(low));
			  double b = ((double)sortedArray.get(high) - (double)sortedArray.get(low));
			  mid = low + 
						(int)Math.ceil(a / 
								b * (high - low - 1));
			  
		 
		   if (sortedArray.get(mid) < toFind)
		    low = mid + 1;
		   else if (sortedArray.get(mid) > toFind)
		    // Repetition of the comparison code is forced by syntax limitations.
		    high = mid - 1;
		   else
		    return mid;
		  }
		 
		  if (sortedArray.get(low) == toFind)
		   return low;
		  else
		   return -1; // Not found
		 }
	public static void printEverything()
	{
		System.out.println(" ");
		System.out.println("::: DATA ::: ");
		System.out.println("Results: ");
		System.out.println("Number of found values: " + valuesFoundGlobal);
		System.out.println("Number of values not found: " + valuesNotFoundGlobal);
		System.out.println("Number of illegal values found: " + badValues);
		System.out.println(" ");
		System.out.println("Time elapsed in nanoseconds for experiments: : : ");
		System.out.println("1 :" + e1t);
		System.out.println("2 :" + e2t);
		System.out.println("3 :" + e3t);
		System.out.println("4 :" + e4t);
		System.out.println("5 :" + e5t);
		System.out.println("End of testing. -- ");
	}
	
}